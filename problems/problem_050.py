# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def half_list(list):
    list2 = []
    list3 = []
    for i in range(len(list)):
        if i < len(list)/2:
            list2.append(list[i])
        else:
            list3.append(list[i])
    return [list2, list3]

input = [1, 2, 3]
print(half_list(input))
