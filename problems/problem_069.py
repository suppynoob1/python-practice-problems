# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# There is pseudocode for you to guide you.


class Student:
    def __init__(self, name):
        self.name = name
        self.scores = []
# class Student
    # method initializer with required state "name"
        # self.name = name
        # self.scores = [] because its an internal state

    def add_score(self, score):
        self.scores.append(score)
    # method add_score(self, score)
        # appends the score value to self.scores

    def get_average(self):
        sum = 0
        if not self.scores:
            return
        for i in self.scores:
            sum += i
        return sum/(len(self.scores))
    # method get_average(self)
        # if there are no scores in self.scores
            # return None
        # returns the sum of the scores divided by
        # the number of scores

dummy = [1,2,3,4,5,6]
student = Student("John")
for i in dummy:
    student.add_score(i)
print(student.get_average())
