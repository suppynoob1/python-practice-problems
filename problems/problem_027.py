# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if not values:
        return

    max = 0
    for i in values:
        if i > max:
            max = i
    return max

dumArr = [1,2,3,4,5]
print(max_in_list(dumArr))
