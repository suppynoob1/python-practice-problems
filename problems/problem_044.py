# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    ret = []
    for key in key_list:
            ret.append(dictionary.get(key))
    return ret

keys =    ["name", "age"]
dictionary = {"name": "Noor", "age": 29}

keys2 =       ["eye color", "age"]
dictionary2 = {"name": "Noor", "age": 29}

keys3 =       ["age", "age", "age"]
dictionary3 = {"name": "Noor", "age": 29}
print(translate(keys, dictionary))
print(translate(keys2, dictionary2))
print(translate(keys3, dictionary3))
