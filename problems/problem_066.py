# Write a class that meets these requirements.
#
# author:       Book
#
# Required state:
#    * author author, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author author»"
#    * get_title:  should return "Title: «title»"
#
# Example:
#    book = Book("Natalie Zina Walschots", "Hench")
#
#    print(book.get_author())  # prints "Author: Natalie Zina Walschots"
#    print(book.get_title())   # prints "Title: Hench"
#
# There is pseudocode availabe for you to guide you


# class Book
class Book:
    def __init__(self, author, title):
        self.author = author
        self.title = title
        # method initializer method with required state
        # parameters author and title
        # set self.author = author
        # set self.title = title

    def get_author(self):
        return "Author: " + self.author
    # method get_author(self)
        # returns "Author: " + self.author

    def get_title(self):
        return "title: " + self.title
    # method get_title(self)
        # returns "Title: " + self.title

book = Book("John", "Wick")
print(book.get_author())
print(book.get_title())
