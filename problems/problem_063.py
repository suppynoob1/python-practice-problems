# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    res = ""

    for i in string:
        asci = (ord(i) + 1)
        if asci > 122:
            asci -= 26
        elif 90 < asci < 97:
            asci -= 26
        asci2 = (chr(asci))
        res += asci2
    return res

inputs =  "zap"
inputs1 = "Kala"
inputs2 = "ABBA"
print(shift_letters(inputs))
print(shift_letters(inputs1))
print(shift_letters(inputs2))
