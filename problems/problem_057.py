# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(num):
    retStr = ''

    for i in range(1, num + 1):
        if i > 1:
            retStr += " + "

        num = i
        den =  i + 1
        retStr += f"{num}/{den}"

    return retStr

print(sum_fraction_sequence(5))
