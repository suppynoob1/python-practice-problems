# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x = 0, y = 0):
    if x >= 0 and x <= 10:
        if(y >= 0 and y <=10):
            return True
        else:
            return False
    else:
        return False

print(is_inside_bounds(-1,1))
