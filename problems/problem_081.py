# Write four classes that meet these requirements.
#
# Name:       Animal
#

class Animal:
    def __init__(self, legs, color):
        self.legs = legs
        self.color = color

    def describe(self):
        return self.__class__.__name__ + " has " + str(self.legs) + " legs and is primarily " + self.color

class Dog(Animal):
    def __init__(self, color = "brown"):
        super().__init__(4, color)
    def speak(self):
        return "Bark!"

class Cat(Animal):
    def __init__(self, color = "black"):
        super().__init__(4, color)
    def speak(self):
        return "Miao!"

class Snake(Animal):
    def __init__(self, color = "tan"):
        super().__init__(0, color)
    def speak(self):
        return "Sssssss!"
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"

dog = Dog("brown")
cat = Cat("black")
snake = Snake("tan")
print(dog.describe(), cat.describe(), snake.describe())
print(dog.speak(), cat.speak(), snake.speak())
