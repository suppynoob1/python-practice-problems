import math
# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!
def safe_divide(numerator=0, denominator=0):
    if denominator == 0:
        return math.inf
    else:
        return numerator/denominator


print(safe_divide())
