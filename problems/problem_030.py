# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <2:
        return
    high = max(values[0],values[1])
    second = min(values[0],values[1])

    for i in range(2, len(values)):
        if values[i] > high:
            second = high
            high = values[i]
        elif values[i] > second and values[i] != high:
            second = values[i]
    return second

dumArr = [3,7,233,233,5,9,3,4,5,6]
print(find_second_largest(dumArr))
