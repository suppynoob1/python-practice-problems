# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(list):
    if len(list) < 2:
        return

    gap = 0
    pointer = 0
    while (pointer < len(list) - 1):
        diff = (max(list[pointer], list[pointer + 1]) - min(list[pointer], list[pointer + 1]))
        if gap < diff:
            gap = diff
        pointer = pointer + 1
    return gap
input1=  [1, 3, 5, 7]
input2=  [1, 11, 9, 20, 0]
input3=  [1, 3, 100, 103, 106]
print(biggest_gap(input1))
print(biggest_gap(input2))
print(biggest_gap(input3))
