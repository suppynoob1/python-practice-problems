import string
# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    if len(password) < 6 or len(password) > 12:
        return "Password not long enough"
    upper = 0
    lower = 0
    digit = 0
    special = 0

    for i in password:
        if i.isdigit():
            digit += 1
        if i.islower():
            lower += 1
        if i.isupper():
            upper += 1
        if i in string.punctuation:
            special += 1
    if upper and lower and digit and special:
        return "Valid Password"
    return "Password is invalid"

password = "Watermelon1!"
print (check_password(password))
