# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    ret = []
    for i in csv_lines:
        splitArr = i.split(",")  # ['8', '1', '7'], ['10', '10', '10'], ['1', '2', '3']

        sum = 0
        for j in splitArr:
            sum += int(j)
        ret.append(sum)

    return ret

dummy = ["8,1,7", "10,10,10", "1,2,3"]
print(add_csv_lines(dummy))


# def add_csv_lines(csv_lines):
#     declare return variable
#     iterate through list1:
#         note: we have a nested list in a list so we have to iterate through the nested list as well
#         note: nested list has CSV as elements so we will use .split(",") to separate them.

#         we will want sum of each nested list, so declare sum variable to add to.
#         iterate through nested list:
#             update sum with current iteration
#         append sum to return list
#     return return variable
