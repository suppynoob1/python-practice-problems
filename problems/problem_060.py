# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

# def function(list):
#     declare result variable

#     iterate through elements of list
#         check if element is odd
#             append to result variable

#     return result

def only_odds(list):
    res = []
    for i in list:
        if i % 2 == 1:
            res.append(i)
    return res

input =   [1, 3, 5, 7]
print(only_odds(input))
