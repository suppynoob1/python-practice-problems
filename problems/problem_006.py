# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age):
    if age < 18:
        form = input("Have you filled out a consent form?")
        if form.lower() == "yes":
            return True
        else:
            return False
    else:
        return True

age = int(input("what is your age?"))

res = can_skydive(age)
print(res)
