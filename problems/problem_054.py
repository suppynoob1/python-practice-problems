# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError

def check_input(any):
    if any == "raise":
        raise ValueError
    return any


input1=   "raise"
input2=   "this is a string"
input3=   3

print(check_input(input3))
print(check_input(input2))
print(check_input(input1))
