# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value
    easy: you're just gonna want to grab value 1 --> compare to value 2 --> if bigger? return other, else return this

### 04 max_of_three

### 06 can_skydive

### continue to plan each
